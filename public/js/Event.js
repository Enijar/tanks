class Event {
    constructor() {
        this.events = {};
    }

    on(event, callback) {
        this.events[event] = callback;
    }

    fire(event, data = {}) {
        if (this.events.hasOwnProperty(event)) {
            this.events[event](data);
        }
    }

    off(event) {
        if (this.events.hasOwnProperty(event)) {
            delete this.events[event];
        }
    }
}
