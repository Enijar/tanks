class Control {
    constructor(props) {
        Object.assign(this, props);

        document.addEventListener('click', (event) => {
            let x, y;

            if (event.pageX || event.pageY) {
                x = event.pageX;
                y = event.pageY;
            } else {
                x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }

            x -= this.canvas.offsetLeft;
            y -= this.canvas.offsetTop;

            props.Event.fire('click', {x, y});
        });
    }
}
