class Game {
    constructor(props) {
        Object.assign(this, props);

        this.canvas.width = this.map.width;
        this.canvas.height = this.map.height;
        this.context = this.canvas.getContext('2d');
        this.Event = new Event(this);
        this.Control = new Control(this);
        this.Map = new Map(this);

        this.render();
    }

    render() {
        requestAnimationFrame(() => this.render());

        this.context.clearRect(0, 0, this.map.width, this.map.height);

        this.Map.render();
    }
}
